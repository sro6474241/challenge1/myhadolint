FROM ubuntu:23.10 AS bin
RUN apt-get update -y && \
apt-get install --no-install-recommends -y \
    wget=1.21.3-1ubuntu1 \
    ca-certificates=20230311ubuntu1 && \
wget --progress=dot:giga \
    https://github.com/hadolint/hadolint/releases/download/v2.12.0/hadolint-Linux-x86_64 && \
chmod +x hadolint-Linux-x86_64 && \
mv hadolint-Linux-x86_64 hadolint

FROM alpine:3.18.4
COPY --from=bin hadolint /bin/
CMD ["/bin/hadolint","-"]
